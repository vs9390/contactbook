from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.serializers import ModelSerializer
from accounts.api.serializers import UserDetailSerializer
from contacts.models import Contact



contact_detail_url = HyperlinkedIdentityField(
        view_name='contacts:detail',
        lookup_field='id'
        )


class ContactListSerializer(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    url = contact_detail_url
    class Meta:
        model = Contact
        fields = [
            'user',
            'id',
            'avatar',
            'name',
            'email',
            'phone_number',
            'date_of_birth',
            'url',
        ]



class ContactDetailSerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = [
            'id',
            'name',
            'avatar',
            'email',
            'phone_number',
            'date_of_birth',
        ]

class ContactCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = [
            'id',
            'name',
            'email',
            'avatar',
            'phone_number',
            'date_of_birth',
        ]