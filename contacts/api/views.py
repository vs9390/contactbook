from django.db.models import Q
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, RetrieveUpdateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from contacts.api.permissions import IsOwner
from contacts.api.serializers import ContactListSerializer, ContactCreateUpdateSerializer, ContactDetailSerializer
from contacts.models import Contact


class ContactListAPIView(ListAPIView):
    serializer_class = ContactListSerializer
    filter_backends= [SearchFilter, OrderingFilter]
    permission_classes = [IsAuthenticated]
    search_fields = ['name', 'email', 'phone_number']

    def get_queryset(self, *args, **kwargs):
        user = self.request.user
        queryset_list = Contact.objects.filter(user=user)
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                    Q(name__icontains=query)|
                    Q(email__icontains=query)|
                    Q(phone_number__first_name__icontains=query)
                    ).distinct()
        return queryset_list

class ContactCreateAPIView(CreateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactCreateUpdateSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class ContactDetailAPIView(RetrieveAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactDetailSerializer
    lookup_field = 'id'
    permission_classes = [IsOwner,IsAuthenticated]


class ContactUpdateAPIView(RetrieveUpdateAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactCreateUpdateSerializer
    lookup_field = 'id'
    permission_classes = [IsOwner,IsAuthenticated]
    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

class ContactDeleteAPIView(DestroyAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactDetailSerializer
    lookup_field = 'id'
    permission_classes = [IsOwner, IsAuthenticated]
