from django.conf.urls import url

from .views import (
    ContactListAPIView,
    ContactCreateAPIView,
    ContactDetailAPIView,
    ContactUpdateAPIView,
    ContactDeleteAPIView
    )




urlpatterns = [
    url(r'^$', ContactListAPIView.as_view(), name='list'),
    url(r'^create/$', ContactCreateAPIView.as_view(), name='create'),
    url(r'^(?P<id>\d+)/$', ContactDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<id>\d+)/edit/$', ContactUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<id>\d+)/delete/$', ContactDeleteAPIView.as_view(), name='delete'),
]