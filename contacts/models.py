from django.core.validators import RegexValidator
from django.db import models
from django.conf import settings

class Contact(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    name = models.CharField(max_length=120)
    avatar = models.CharField(max_length=20, default='svg-1')
    email = models.EmailField(max_length=254)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=15, blank=True)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return self.name