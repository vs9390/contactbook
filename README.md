# Contactbook
Contactbook is a Django, Django-rest-framework, angularjs and angularjs powered contact manager.

## Requirements
  - Python3
  - Django
  - Django rest framework
  - Angular js
  - Angular material design
  - SqLite3

### Installation

Install virtualenv and create virtualenv with python3.
```sh
$ pip install virtualenv
$ virtualenv py3 -p /usr/local/bin/python3.4
$ source py3/bin/activate
```

Clone Repo and install dependencies
```sh
$ git clone https://bitbucket.org/vs9390/contactbook.git
$ cd contactbook
$ pip install -r requirments.txt
```

Create Database and run server
```sh
$ python manage.py migrate
$ python manage.py runserver
```