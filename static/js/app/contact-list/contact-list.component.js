'use strict';

angular.module('contactList')
    .config(($mdIconProvider, $mdThemingProvider) => {
    // Register the user `avatar` icons
    $mdIconProvider
      .defaultIconSet('static/img/svg/avatars.svg', 128)
      .icon("menu", "static/img/svg/menu.svg", 24)
      .icon("add", "static/img/svg/add.svg", 48)
      .icon("email", "static/img/svg/email.svg", 24)
      .icon("dob", "static/img/svg/dob.svg", 24)
      .icon("twitter", "static/img/svg/twitter.svg", 24)
    .icon("logout", "static/img/svg/logout.svg", 24)
      .icon("phone", "static/img/svg/phone.svg", 24);

    $mdThemingProvider.theme('default')
      .primaryPalette('blue')
      .accentPalette('red');
  })
    .component('contactList', {
        templateUrl: '/api/templates/contact-list.html',

        controller: function( $cookies, $location, $routeParams, $rootScope, $scope, $log, $http, $mdDialog, $mdMedia,$mdSidenav, $mdToast){



            Date.prototype.yyyymmdd = function() {
              var mm = this.getMonth() + 1; // getMonth() is zero-based
              var dd = this.getDate();

              return [this.getFullYear(), mm, dd].join('-'); // padding
            };


            if ($cookies.get('token')) {
                console.log('ALLOW');
            }
            else {
               console.log('DENY : Redirecting to Login');
              $location.path('/login');
                window.location.reload()
            }


            $scope.logout = function(){
                 $location.path('/logout');
                window.location.reload()
            }



            $http.defaults.headers.common.Authorization = 'JWT ' + $cookies.get("token")

            $http({
              method: 'GET',
              url: '/api/contacts/'
            }).then(function successCallback(response) {

                $scope.users = response.data;
                $scope.selected = $scope.users[0];
              }, function errorCallback(response) {
                var status = response.status;

                    if (status == 401 || status == 403) {
                        $location.path('/logout');
                        window.location.reload()
                    }

              });



                $scope.toggleUsersList = function() {
                    $mdSidenav('left').toggle();
                }


                $scope.showAdd = function(ev) {
                    $mdDialog.show({
                      template: '<md-dialog aria-label="Mango (Fruit)"> <md-content class="md-padding"> <form name="userForm"> <div layout layout-sm="column"> <md-input-container flex> <label>First Name</label> <input ng-model="user.firstName" placeholder="Placeholder text"> </md-input-container> <md-input-container flex> <label>Last Name</label> <input ng-model="theMax"> </md-input-container> </div> <md-input-container flex> <label>Address</label> <input ng-model="user.address"> </md-input-container> <div layout layout-sm="column"> <md-input-container flex> <label>City</label> <input ng-model="user.city"> </md-input-container> <md-input-container flex> <label>State</label> <input ng-model="user.state"> </md-input-container> <md-input-container flex> <label>Postal Code</label> <input ng-model="user.postalCode"> </md-input-container> </div> <md-input-container flex> <label>Biography</label> <textarea ng-model="user.biography" columns="1" md-maxlength="150"></textarea> </md-input-container> </form> </md-content> <div class="md-actions" layout="row"> <span flex></span> <md-button ng-click="answer(\'not useful\')"> Cancel </md-button> <md-button ng-click="answer(\'useful\')" class="md-primary"> Save </md-button> </div></md-dialog>',
                      targetEvent: ev,
                    })
                    .then(function(answer) {
                      $scope.alert = 'You said the information was "' + answer + '".';
                    }, function() {
                      $scope.alert = 'You cancelled the dialog.';
                    });
                };




                $scope.selectUser = function (user) {
                        $scope.selected = user;
                        $scope.selectedUser = user;

                        this.tabIndex = 0;
                    };


                var AddUserDialogController = (function () {
                    function AddUserDialogController($mdDialog) {
                        this.$mdDialog = $mdDialog;
                        this.avatars = [
                            'svg-1', 'svg-2', 'svg-3', 'svg-4', 'svg-5', 'svg-6', 'svg-7', 'svg-8', 'svg-9', 'svg-10', 'svg-11', 'svg-12', 'svg-13','svg-14','svg-15','svg-16'
                        ];
                    }
                    AddUserDialogController.prototype.cancel = function () {
                        this.$mdDialog.cancel();
                    };
                    AddUserDialogController.prototype.save = function () {
                        this.$mdDialog.hide(this.user);
                    };
                    AddUserDialogController.$inject = ['$mdDialog'];
                    return AddUserDialogController;
                })();

                var CreateUser = (function () {
                    function CreateUser(firstName, lastName, avatar, email, date_of_birth, phone_number) {
                        this.firstName = firstName;
                        this.lastName = lastName;
                        this.avatar = avatar;
                        this.email = email;
                        this.date_of_birth = date_of_birth.yyyymmdd();
                        this.phone_number = phone_number;
                    }
                    return CreateUser;
                })();

                var User = (function () {
                    function User(name, avatar, email, date_of_birth, phone_number) {
                        this.name = name;
                        this.avatar = avatar;
                        this.email = email;
                        this.phone_number = phone_number;
                        this.date_of_birth = date_of_birth.yyyymmdd();
                    }
                    User.fromCreate = function (user) {
                        return new User(user.firstName + ' ' + user.lastName, user.avatar, user.email, user.date_of_birth, user.phone_number);
                    };
                    return User;
                })();

              var dialogTemp = '<md-dialog>' +
                      '<form name="userForm" ng-submit="ctrl.save()">' +
                       ' <md-dialog-content>' +
                         ' <div layout="column" layout-padding>' +
                        '    <div layout-gt-sm="row">' +
                       '       <md-input-container class="md-block" flex-gt-sm>' +
                            '    <label>Avatar</label>' +
                           '     <md-select ng-model="ctrl.user.avatar" name="avatar" required>' +
                          '        <md-option ng-repeat="avatar in ctrl.avatars" ng-value="avatar">' +
                         '           <md-icon md-svg-icon="{{ avatar }}"></md-icon>' +
                              '    </md-option>' +
                             '   </md-select>' +
                            '    <div ng-messages="userForm.avatar.$error">' +
                             '     <div ng-message="required">This is required.</div>' +
                            '    </div>' +
                           '   </md-input-container>' +
                          '    <md-input-container class="md-block" flex-gt-sm>' +
                             '   <label>First name</label>' +
                            '    <input ng-model="ctrl.user.firstName" name="firstName" required>' +
                           '     <div ng-messages="userForm.firstName.$error">' +
                          '        <div ng-message="required">This is required.</div>' +
                         '       </div>' +
                        '      </md-input-container>' +
                       '       <md-input-container class="md-block" flex-gt-sm>' +
                      '          <label>Last name</label>' +
                     '           <input ng-model="ctrl.user.lastName" name="lastName" required>' +
                             '   <div ng-messages="userForm.lastName.$error">' +
                            '      <div ng-message="required">This is required.</div>' +
                           '     </div>' +
                          '    </md-input-container>' +
                         '   </div>' +
                      '<div layout-gt-sm="row">'+
                  '<md-input-container>'+
                            '  <label>Email</label>'+
                        '  <input ng-model="ctrl.user.email" type="email" name="email">'+
                     '         <div ng-messages="userForm.bio.$error">' +
                         '         <div ng-message="required">This is required.</div>' +
                        '        </div>' +
                       '     </md-input-container>' +
                      '<md-input-container>'+
            '<label>Date of Birth</label>'+
            '<md-datepicker ng-model="ctrl.user.date_of_birth"></md-datepicker>'+
                       '         <div ng-messages="userForm.bio.$error">' +
                         '         <div ng-message="required">This is required.</div>' +
                        '        </div>' +
          '</md-input-container>'+


                    '  <md-input-container class="md-block" flex-gt-sm>'+
                   '   <label>Phone Number</label>'+
                   '   <input type="number" name="phone" ng-model="ctrl.user.phone_number" require  />'+

                    '  <div class="hint" ng-show="showHints">10 Digits</div>'+

                     ' <div ng-messages="userForm.phone.$error" ng-hide="showHints">'+
                    '    <div ng-message="pattern"> Please enter a valid phone number.</div>'+
                    '  </div>'+
                   ' </md-input-container>'+
                      ' </div>'+

                      '    </div>' +
                     '   </md-dialog-content>' +
                     '   <md-dialog-actions layout="row">' +
                         ' <md-button ng-click="ctrl.cancel()">' +
                        '    Cancel' +
                        '  </md-button>' +
                        '  <md-button type="submit" class="md-primary md-raised"' +
                        '             ng-disabled="userForm.$invalid">' +
                        '    Save' +
                       '   </md-button>' +
                      '  </md-dialog-actions>' +
                     ' </form>' +
                    '</md-dialog>';

                $scope.removeUser = function (user) {
                        var id = $scope.selected.id;
                        var index = $scope.users.indexOf(user);
                        $scope.users.splice(index, 1);
                        $scope.selected = $scope.users[0];
                        $mdToast.showSimple('Contact Deleted');

                        $http({
                                  method: 'DELETE',
                                  url: '/api/contacts/'+id+'/delete/'
                                }).then(function successCallback(response) {

                                    console.log(response.data);
                                  }, function errorCallback(response) {

                                        $mdToast.showSimple('Error in connecting to server.');
                        });


                };

                $scope.addUser = function ($event) {
                        var self = this;
                        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'));
                        $mdDialog.show({
                            template: dialogTemp,
                            parent: angular.element(document.body),
                            targetEvent: $event,
                            controller: AddUserDialogController,
                            controllerAs: 'ctrl',
                            clickOutsideToClose: true,
                            fullscreen: useFullScreen
                        }).then(function (user) {
                            var newUser = User.fromCreate(user);
                            //self.users.push(newUser);
                            //self.selectUser(newUser);
                            $mdToast.showSimple('New Contact Added.');


                                $http({
                                  method: 'POST',
                                  url: '/api/contacts/create/',
                                  data : angular.toJson(newUser)
                                }).then(function successCallback(response) {

                                    $http({
                                      method: 'GET',
                                      url: '/api/contacts/'
                                    }).then(function successCallback(response) {

                                        $scope.users = response.data;
                                        $scope.selected = $scope.users[0];
                                      }, function errorCallback(response) {
                                        var status = response.status;

                                            if (status == 401 || status == 403) {
                                                $location.path('/logout');
                                                window.location.reload()
                                            }

                                      });

                                    console.log(response.data);
                                  }, function errorCallback(response) {

                                        $mdToast.showSimple('Error in connecting to server.');
                                  });



                            console.log(angular.toJson(newUser));
                        }, function () {
                            console.log('You cancelled the dialog.');
                        });
                    };

        }
    });


