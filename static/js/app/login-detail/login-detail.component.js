'use strict';

angular.module('loginDetail').
    component('loginDetail', {
        templateUrl: '/api/templates/login-detail.html',
        controller: function(
                $cookies, 
                $http, 
                $location, 
                $routeParams, 
                $rootScope, 
                $scope
            ){
            var loginUrl = '/api/users/login/'
            $scope.loginError = {}
            $scope.user = {

            }

            $scope.$watch(function(){
                if ($scope.user.password) {
                    $scope.loginError.password = ""
                } else if ($scope.user.username) {
                    $scope.loginError.username = ""
                }
            })



            var tokenExists = $cookies.get("token")

            if (tokenExists) {
                $location.path('/contact');
                window.location.reload()
            }



            $scope.doLogin = function(user){
                if (!user.username) {
                    $scope.loginError.username = ["Username may not be blank."]
                } 

                if (!user.password) {
                    $scope.loginError.password = ["Password is required."]
                }

                if (user.username && user.password) {
                    var reqConfig = {
                        method: "POST",
                        url: loginUrl,
                        data: {
                            username: user.username,
                            password: user.password
                        },
                            headers: {}
                    }
                    var requestAction = $http(reqConfig)
                    
                    requestAction.success(function(r_data, r_status, r_headers, r_config){

                            $cookies.put("token", r_data.token)
                            $cookies.put("username", r_data.username)

                            $location.path("/")
                            window.location.reload()
                    })
                    requestAction.error(function(e_data, e_status, e_headers, e_config){
                            console.log(e_data) // error
                            $scope.loginError = e_data

                    })
                }
                

            }
            // $http.contact()
        }
})
