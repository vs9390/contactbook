'use strict';

angular.module('try')

    .config(
        function(
          $locationProvider,
          $resourceProvider,
          $routeProvider
          ){


          $locationProvider.html5Mode({
              enabled:true
            })

          $resourceProvider.defaults.stripTrailingSlashes = false;

          $routeProvider.
              when("/", {
                redirectTo: '/contact'
              }).
              when("/contact", {
                  template: "<contact-list></contact-list>"
                  // redirectTo: '/'
              }).
              when("/login", {
                  template: "<login-detail></login-detail>"
              }).
              when("/logout", {
                  template: "<logout-detail></logout-detail>"
              }).
               when("/register", {
                  template: "<register-detail></register-detail>"
              }).
              otherwise({
                  template: "Not Found"
              })

    });