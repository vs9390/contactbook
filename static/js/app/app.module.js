'use strict';

angular.module('try', [
    // external
    'angularUtils.directives.dirPagination',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ng-showdown',
    'ui.bootstrap',
    // internal
    'contactList',
    'loginDetail',
    'logoutDetail',
    'registerDetail',

]);