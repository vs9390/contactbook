'use strict';

angular.module('logoutDetail').
    component('logoutDetail', {
        templateUrl: '/api/templates/logout-detail.html',
        controller: function(
                $cookies,
                $location,
                $scope
            ){


            var tokenExists = $cookies.get("token")
            if (tokenExists) {
                // verify token
                $scope.loggedIn = true;
                $cookies.remove("token")
                $scope.user = {
                    username: $cookies.get("username")
                }
                $location.path('/login')
                window.location.reload()
            }else {


                $location.path('/login')
                window.location.reload()
            }



        }
})
